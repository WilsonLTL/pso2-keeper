import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'

Vue.config.productionTip = false;
let VueCookie = require('vue-cookie');
Vue.use(VueCookie);

new Vue({
  render: h => h(App),
}).$mount('#app')
