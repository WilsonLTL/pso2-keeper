FROM node:latest

COPY . .
WORKDIR .
RUN npm install
RUN npm run build
RUN npm install -g http-server
EXPOSE 8080
CMD ["http-server"]

